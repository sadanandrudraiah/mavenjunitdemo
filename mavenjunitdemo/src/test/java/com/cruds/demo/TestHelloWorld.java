package com.cruds.demo;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestHelloWorld {

	@Test
	public void test() {
		
		HelloWorld obj = new HelloWorld();
		
		assertTrue(obj.sayHello());
		
	}

}
